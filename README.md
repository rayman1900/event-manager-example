# README #

2D Unity Example of an Event Manager based upon the codes provided by Shavais Zarathu.

### What is this repository for? ###

* This project demonstrate how a Unity2D project can implement a simple Event Manager system.
* 0.0.1a
* Based on Shavais Zarathu Event Manager (shavais@gmail.com) and made by Carl Beaumier (carlbeaumier@gmail.com)

### How do I play? ###

- Move with "a" or "d" key to go left or right.
- Press E to interact. A prompt will appear on top of the head of the character when the action is available.