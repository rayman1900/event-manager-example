﻿using UnityEngine;
using System.Collections;

public class SomeArgs
{
	public int SomeIntParameter;
	public string SomeStringParameter;
	public object SomeParameterThatMightBeAClass;
	public bool SomeBool;
}
