﻿/*
 * Event Manager by Shavais Zarathu
 * 
 * Ok, I'll paste in here this very basic sample implementation I just whipped up. 
 * It compiles, but I haven't tested it so it might have run time bugs, but it's pretty straight forward, so I doubt it. 
 * I don't know anything about what you're doing, so, of course it may need customizing for your particular needs. 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

// A namespace to put miscellaneous classes in that are intended to be used by any game.
namespace GameLib
{
	// A class method that is passed to RegisterListener must have this signature
	public delegate void EventListener(object sender, object args);
	
	/*
    Note that this very basic implementation does not provide any direct feedback channel from event listeners back to an event raiser.
    It also does not provide any mechanism to allow event listeners to cancel further processing of an event.
    There are, of course, fairly simple and obvious ways to alter the implementation to do either or both of those things, if needed.
    */
	public static class EventManager
	{
		//private variables
		private static Dictionary<string, List<EventListener>> listeners = new Dictionary<string, List<EventListener>>();

		/// <summary>
		/// Registers the listener.
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="listener">Listener.</param>
		public static void RegisterListener(string eventName, EventListener listener)
		{
			if (!listeners.ContainsKey(eventName)) listeners[eventName] = new List<EventListener>();
			
			// if you want to be able to register the same method for the same event multiple times, so it gets called multiple times for the save event, comment out this line:
			if (listeners[eventName].Contains(listener)) return;
			
			listeners[eventName].Add(listener); // note that when the event is raised, the registered listeners will be called in the order they were added.
		}

		/// <summary>
		/// Removes the listener.
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="listener">Listener.</param>
		public static void RemoveListener(string eventName, EventListener listener)
		{
			if (listeners.ContainsKey(eventName))
			{
				if (listeners[eventName].Contains(listener))
				{
					listeners[eventName].Remove(listener);
				}
			}
		}

		/// <summary>
		/// Raises the event.
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="sender">Sender.</param>
		/// <param name="args">Arguments.</param>
		public static void RaiseEvent(string eventName, object sender, object args)
		{
			if (!listeners.ContainsKey(eventName)) return;

			foreach(EventListener listener in listeners[eventName])
			{
				// call the registered listener
				listener(sender, args);
			}
		}
	}
}
