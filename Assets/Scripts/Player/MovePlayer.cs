﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {

	private Rigidbody2D rigidBody2D;
	private float h;
	private float v;
	public float speed = 10f;

	void Awake(){
		rigidBody2D = GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		h = Input.GetAxis ("Horizontal");
		
		Move (rigidBody2D, speed, h);

	}

	/// <summary>
	/// Move the specified rb2D, speed, xAxis and yAxis.
	/// </summary>
	/// <param name="rb2D">Rb2 d.</param>
	/// <param name="speed">Speed.</param>
	/// <param name="xAxis">X axis.</param>
	/// <param name="yAxis">Y axis.</param>
	private void Move(Rigidbody2D rb2D, float speed, float xAxis)
	{
		//Managing the horizontal movement
		if (Mathf.Abs(xAxis) > 0) {
			rb2D.AddForce(transform.right * speed * xAxis);
		}
	}
}
